package com.jcoding.rest;

import com.jcoding.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest("server.port=9000")
public class HelloWorldRestFacadeIntegrationTest {

    private RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void shouldReturnHelloWorldWhenHelloCalled() {
        ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:9000/hello", String.class);

        assertThat(entity.getStatusCode().is2xxSuccessful(), is(true));
        assertThat(entity.getBody(), is(equalTo("hello world!")));
    }
}