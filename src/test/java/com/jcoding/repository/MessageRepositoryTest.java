package com.jcoding.repository;

import com.jcoding.App;
import com.jcoding.domain.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(App.class)
@ActiveProfiles("test")
public class MessageRepositoryTest {

    @Autowired
    MessageRepository messageRepository;

    @Test
    public void shouldFindMessageWithContentTest2() {
        Message message = messageRepository.findByMessageContent("Test2");
        assertThat(message.getId(), equalTo(2L));
    }
}
