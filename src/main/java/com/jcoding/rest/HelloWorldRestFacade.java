package com.jcoding.rest;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Component
@Path("/")
public class HelloWorldRestFacade {

    @GET
    public String advice() {
        return "try calling http://localhost:8080/hello";
    }

    @GET
    @Path("hello")
    public String hello() {
        return "hello world!";
    }
}
