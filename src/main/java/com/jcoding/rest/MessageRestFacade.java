package com.jcoding.rest;

import com.jcoding.domain.Message;
import com.jcoding.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.ok;

@Component
@Path("message")
public class MessageRestFacade {

    @Autowired
    public MessageRepository messageRepository;

    @GET
    @Path("addmessage")
    public Response addMessage(@QueryParam("message") String message) {
        messageRepository.save(new Message(message));
        return ok("Successfully added message", TEXT_PLAIN).build();
    }

    @GET
    @Produces("application/json")
    @Path("findall")
    public Iterable<Message> findAll() {
        return messageRepository.findAll();
    }

    @GET
    @Produces("application/json")
    @Path("findbymessage")
    public Message findByMessage(@QueryParam("message") String message) {
        return messageRepository.findByMessageContent(message);
    }
}
