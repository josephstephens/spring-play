package com.jcoding.repository;

import com.jcoding.domain.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {

    Message findByMessageContent(String messageContent);
}
