package com.jcoding.config;

import com.jcoding.rest.HelloWorldRestFacade;
import com.jcoding.rest.MessageRestFacade;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(HelloWorldRestFacade.class);
        register(MessageRestFacade.class);
    }
}
